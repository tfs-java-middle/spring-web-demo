package com.example.springwebdemo.service.inn;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Клиент для взаимодействия с РКН
 */
public interface InnApiClient {

    @GetMapping("/{inn}/taxes")
    List<Tax> getTaxes(@PathVariable String inn);

    @PostMapping("/{inn}/taxes")
    Boolean registerTax(@PathVariable String inn, @RequestBody Tax tax);

}
