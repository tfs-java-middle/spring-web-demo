package com.example.springwebdemo.service.inn;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.Logger;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.openfeign.support.SpringMvcContract;

import java.util.concurrent.TimeUnit;

//@Configuration
@RequiredArgsConstructor
public class ApiConfig {

    private final ObjectMapper objectMapper;

    //@Bean
    public InnApiClient innApiClient() {

        return Feign.builder()
                .encoder(new JacksonEncoder(objectMapper))
                .decoder(new JacksonDecoder(objectMapper))
                .contract(new SpringMvcContract())
//                .requestInterceptor(fnsInterceptor)
//                .logger(new FeignSlf4jMultilineLogger(ReceiptFnsOperations.class))
                .logLevel(Logger.Level.FULL)
                .options(new feign.Request.Options(10, TimeUnit.SECONDS, 10, TimeUnit.SECONDS, true))
                .target(InnApiClient.class, "");
    }
}
