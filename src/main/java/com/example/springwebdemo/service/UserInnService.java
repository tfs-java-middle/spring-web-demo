package com.example.springwebdemo.service;

import com.example.springwebdemo.service.inn.Tax;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserInnService {

    private final UserService userService;

    @HystrixCommand(fallbackMethod = "defaultTaxes")
    public List<Tax> getTaxes(String inn) {
        return null;
    }

    private List<Tax> defaultTaxes(String username) {
        return Collections.emptyList();
    }
}
