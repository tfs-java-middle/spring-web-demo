package com.example.springwebdemo.service;

import com.example.springwebdemo.dto.User;
import com.example.springwebdemo.exception.UserPhoneUniqueException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class UserService {

    private final AtomicLong idSeq = new AtomicLong(0);
    private final ConcurrentHashMap<Long, User> users = new ConcurrentHashMap<>();

    public User getById(Long id) {
        return users.get(id);
    }

    public User create(User user) {
        validate(user);
        Long id = idSeq.incrementAndGet();
        user.setId(id);
        users.put(id, user);
        return getById(id);
    }

    private void validate(User user) {
        String phone = user.getPhone();
        if (phone != null) {
            Optional<User> findUser = users.values().stream()
                    .filter(user1 -> phone.equals(user1.getPhone()))
                    .findAny();
            if (findUser.isPresent()) {
                throw new UserPhoneUniqueException("Phone 7******" + phone.substring(phone.length() - 4) + " is already exist");
            }
        }
    }

    public User update(User user) {
        Long id = user.getId();
        if (id != null) {
            validate(user);
            users.put(id, user);
        }
        return null;
    }

    public void deleteById(Long id) {
        users.remove(id);
    }

    public void error() {
        throw new RuntimeException("test");
    }
}
