package com.example.springwebdemo.dto;

import com.example.springwebdemo.validation.ValidPhoneNumber;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Schema(description = "Пользователь")
public class User {
    private Long id;
    @NotBlank
    private String name;
    private String surname;
    private String inn;
    @Schema(description = "Номер телефона клиента", required = true)
    @ValidPhoneNumber
    private String phone;
}
