package com.example.springwebdemo.exception;

public class UserPhoneUniqueException extends RuntimeException {
    public UserPhoneUniqueException(String s) {
        super(s);
    }
}
